#include <NewPing.h>
//#include <TonePlayer.h>
#include <Servo.h>

#define tempo 10
#define TRIGGER_PIN  12 // Arduino pin tied to trigger pin on the ultrasonic sensor.
//#define ECHO_PIN     11  // Arduino pin tied to echo pin on the ultrasonic sensor.
//#define MAX_DISTANCE 60 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.
Servo myservo;
Servo myservo2;

//TonePlayer tone1 (TCCR1A, TCCR1B, OCR1AH, OCR1AL, TCNT1H, TCNT1L);  // pin D9 (Uno), D11 (Mega)

NewPing sonar(TRIGGER_PIN, 11, 30); // NewPing setup of pins and maximum distance.

int ledPin = 7;
int frequencia = 0;
int Pinofalante = 9;
int found = false;
int lastv = 0;
int lasth = 0;
int invv = 1;
int invh = 1;
int minlim = 3;
void setup(){
  Serial.begin(115200); // Open serial monitor at 115200 baud to see ping results.
  pinMode(Pinofalante,OUTPUT); //Pino do buzzer
  pinMode(ledPin, OUTPUT);
  myservo.attach(4);
  myservo2.attach(3);  
}
void alertDetected(){
  if(!found){
    found = true;
//    tone1.tone (220);  // 220 Hz
    delay (200);
//    tone1.noTone ();
 //   tone1.tone (440);
    delay (200);
 //   tone1.noTone ();
  //  tone1.tone (880);
    delay (200);  
  }
}
void fire(){
  for (int i = 0; i < 10; i++) {      
    digitalWrite(ledPin, HIGH);
//    tone1.tone(262);
    delay(50);
    digitalWrite(ledPin, LOW);
//    tone1.tone(296);
    delay(50);
  }  
  lastv = random(30,120);
  lasth = random(10,90);  
  Serial.print("Escape ");
  Serial.print(lastv);
  Serial.print(" / ");
  Serial.println(lasth);
  delay(1000);
}
void fireJota(){
    for (frequencia = 120; frequencia < 160; frequencia += 2) {      
  //    tone1.tone (frequencia/2);
      delay(0.0000000001);
      digitalWrite(ledPin, HIGH);
      delay(45);
      digitalWrite(ledPin, LOW);
      delay(45);
    }
    for (frequencia = 160; frequencia > 100; frequencia -= 1){
      //tone1.tone (frequencia);
      //tone(Pinofalante, frequencia);
      delay(0.0000000001);
      digitalWrite(ledPin, HIGH);
      delay(45);
      digitalWrite(ledPin, LOW);
      delay(45);
    }  
}
int searchtimeout = 0;
void searchTarget(){
  if(searchtimeout > 100){
    //tone1.tone (220);
    //tone1.noTone();
    searchtimeout = 0;
  }
  lastv = lastv + invv*random(1,3);
  lasth = lasth + invh*random(1,3);
  if(lastv > 100 || lastv <= 15){
      invv = -invv;
      lastv = (lastv > 100?98:15);
  }
  if(lasth > 170 || lasth <= 2){
      invh = -invh;
      lasth = (lasth > 170?167:3);
  
  }
  myservo.write(lastv);
  myservo2.write(lasth);
    Serial.print("H = ");
    Serial.print(invh); 
    Serial.print("/");
    Serial.print(lasth);
    Serial.print(", V = ");    
    Serial.print(invv);
    Serial.print("/");
    Serial.println(lastv);  
  delay (10);
  searchtimeout++;
}
void suspendFire(){
  found = false;
  //tone1.tone (880);  // 220 Hz
  //delay (100);
  //tone1.noTone ();
  //tone1.tone (440);
  //delay (100);
  //tone1.noTone ();
  //tone1.tone (220);
  //delay (100);  
  //tone1.noTone();  
}
void loop(){
  //delay(50);                     // Wait 50ms between pings (about 20 pings/sec). 29ms should be the shortest delay between pings.
  Serial.print("Ping: ");
  Serial.print(sonar.ping_cm()); // Send ping, get distance in cm and print result (0 = outside set distance range)
  Serial.println("cm");
  
  if (sonar.ping_cm() > 3 ){
    alertDetected();  
    fire();
  }else {
    suspendFire();
    searchTarget();
  }
}






 


