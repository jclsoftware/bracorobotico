# Trash Bot: Um Robô Ecológico

[![Apresentação](https://i9.ytimg.com/vi/Mo9myVQ2B20/mq2.jpg?sqp=CLjcuf0F&rs=AOn4CLCz0n2q3z6v3P9AylUHf7WMk5mvnQ)](https://youtu.be/Mo9myVQ2B20)
[![Apresentação](https://i.ytimg.com/vi/GVdkRPNAbhY/hqdefault_live.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLByKFoIE1k6ficnhpSLY8VHRG7c0Q)](https://www.youtube.com/watch?v=kt-8dG8dr8g)

Este tutorial contém materiais e instruções básicas para construir um robô ecológico, reaproveitando peças que seriam descartadas como lixo.

## Resumo
O lixo eletrônico representa um dos maiores problemas da atualidade, o que motiva a reutilização de seus componentes para promover um consumo mais responsável. Assim, nosso objetivo foi construir um robô a partir de materiais reutilizados combinados com poucos componentes novos. Demonstramos através da robótica como é possível incentivar o reaproveitamento de materiais descartados dando a eles novas utilidades. Para isso, foi escolhido o projeto de um kit de braço robótico. Com algumas peças alternativas obtidas de lixo eletrônico, construímos a versão ecológica do braço robótico, o Trash Bot. Nossos principais resultados são: um protótipo funcional de braço robótico ecológico que detecta e ilumina objetos ao seu redor e instruções para reprodução do projeto. Economizamos 33% do valor que seria investido em um braço robótico com peças compradas. Além disso, as peças reaproveitadas são opções fáceis de encontrar e seu uso promoveu um aprendizado mais completo e sustentável.

[![Abstract](http://img.youtube.com/vi/ci35OWsMtEs/0.jpg)](https://www.youtube.com/watch?v=ci35OWsMtEs "Trash Bot - Abstract")

## Visão geral do projeto
<img src="https://gitlab.com/jclsoftware/bracorobotico/-/raw/master/images/trashbot.jpg" alt="Trash Bot Overview" width="70%" align="center"/>


## Lista de materiais utilizados no projeto

| Quantidade | Descrição                                    | Custo (un) |
|------------|----------------------------------------------|------------|
| 2          | Lâminas de persiana                          | R$ 0,00    |
| 1          | LED de teclado, mouse, PC ou gravador de DVD | R$ 0,00    |
| 10         | Parafusos de gravador de DVD                 | R$ 0,00    |
| 1          | Suporte de Lâmpada fluorescente              | R$ 0,00    |
| 1          | Caixa plástica de fonte de alimentação       | R$ 0,00    |
| 2          | Tubos de caneta esferográfica sem tinta      | R$ 0,00    |
| 2          | Potenciômetros de Radio inutilizado          | R$ 0,00    |
| 1          | Sensor de distância (novo)                   | R$ 15,00   |
| 2          | Servo motores (novos)                        | R$ 10,00   |


## Esquema de montagem do braço robótico
<p align="center">
<img src="https://gitlab.com/jclsoftware/bracorobotico/-/raw/master/images/schema.png" alt="Trash Bot Schema" width="70%" align="center"/>
</p>

## Algoritmo para simulação de autonomia
  - Baseado em busca aleatória por objetos físicos.
  - Detecta e aponta com emissor de luz para um objeto próximo.
  - Código fonte para Arduino.
  [![Source Code](https://gitlab.com/jclsoftware/bracorobotico/-/raw/master/robobra_o.ino)]()
<img src="https://gitlab.com/jclsoftware/bracorobotico/-/raw/master/images/codigo1.png" alt="Trash Bot Source Code 1/3" width="30%"/>  
<img src="https://gitlab.com/jclsoftware/bracorobotico/-/raw/master/images/codigo2.png" alt="Trash Bot Source Code 2/3" width="30%"/>  
<img src="https://gitlab.com/jclsoftware/bracorobotico/-/raw/master/images/codigo3.png" alt="Trash Bot Source Code 3/3" width="30%"/>  

Este material está disponível publicamente sob licença MIT. Ao utilizar o material, por favor cite:

LEÃO, J. J. C. C.; LEÃO J. C. C.; SANTOS, R. J. V.; LEÃO J. C.; Trash Bot: Um Robô Ecológico. Mostra Nacional de Robótica. 2020.

The MIT License (MIT)

Copyright (c) 2020  <a href="https://www.youtube.com/channel/UCLfFL6bdBCKXNO42SJt7j8A" target="_Youtube">Clube da Robótica</a>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


